@extends ('layouts.app')
@section ('content')

        <div class="container " style="padding-top: 40px;padding-bottom: 40px;">
            <h1>O nas</h1>
            <div class="blog-content">
                <div style="float: left; width: 50%; margin-top: 5%">
                    <p>A&O Adrian Sznajder Oskar Sznajder spółka cywilna, jest firmą działającą na rynku nieruchomości od 2017 roku.</p></br>

                    <p>W ciągu ostatnich lat spółka z sukcesem realizowała projekty nieruchomości komercyjnych oraz mieszkaniowych.</p></br>

                    <p>Spółkę wyróżnia doświadczenie zespołu w realizacji przedsięwzięć technologicznych, które optymalizują procesy poszukiwania atrakcyjnych inwestycji, administrowania portfelem, czy docierania bezpośrednio do docelowego klienta.</p></br>
                </div>
                <div style="float: left; width: 50%">
                    <img style="width:50%" src="https://bi.im-g.pl/im/d0/12/16/z23146448Q,Wiezowiec-Atanera-przy-Towarowej.jpg">
                </div>
            </div>
        </div>
        <div style="clear:both"></div>
@endsection
